unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, uniGUIBaseClasses, uniEdit,
  BMUniTriggersButtonEdit, uniLabel;

type
  TMainForm = class(TUniForm)
    uniTriggersButtonEdit1: TBMuniTriggersButtonEdit;
    uniTriggersButtonEdit2: TBMuniTriggersButtonEdit;
    uniTriggersButtonEdit3: TBMuniTriggersButtonEdit;
    uniTriggersButtonEdit4: TBMuniTriggersButtonEdit;
    uniTriggersButtonEdit5: TBMuniTriggersButtonEdit;
    UniEdit1: TUniEdit;
    UniEdit2: TUniEdit;
    UniEdit3: TUniEdit;
    UniEdit4: TUniEdit;
    UniEdit5: TUniEdit;
    UniLabel1: TUniLabel;
    UniLabel2: TUniLabel;
    UniLabel3: TUniLabel;
    uniTriggersButtonEdit6: TBMuniTriggersButtonEdit;
    UniEdit6: TUniEdit;
    procedure uniTriggersButtonEdit1TriggersButtonEvent(Sender: TObject;
      ButtonEvent: string);
    procedure uniTriggersButtonEdit2TriggersButtonEvent(Sender: TObject;
      ButtonEvent: string);
    procedure uniTriggersButtonEdit3TriggersButtonEvent(Sender: TObject;
      ButtonEvent: string);
    procedure uniTriggersButtonEdit4TriggersButtonEvent(Sender: TObject;
      ButtonEvent: string);
    procedure UniEdit1AjaxEvent(Sender: TComponent; EventName: string;
      Params: TStrings);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  uniGUIVars, MainModule, uniGUIApplication;

function MainForm: TMainForm;
begin
  Result := TMainForm(UniMainModule.GetFormInstance(TMainForm));
end;

procedure TMainForm.UniEdit1AjaxEvent(Sender: TComponent; EventName: string;
  Params: TStrings);
var
  A: String;
begin
  A:= EventName;

end;

procedure TMainForm.uniTriggersButtonEdit1TriggersButtonEvent(Sender: TObject;
  ButtonEvent: string);
begin
   showmessage(sender.ClassName  + ' ===>' + ButtonEvent) ;
end;

procedure TMainForm.uniTriggersButtonEdit2TriggersButtonEvent(Sender: TObject;
  ButtonEvent: string);
begin
      showmessage(sender.ClassName  + ' ===>' + ButtonEvent) ;
end;

procedure TMainForm.uniTriggersButtonEdit3TriggersButtonEvent(Sender: TObject;
  ButtonEvent: string);
begin
   showmessage(sender.ClassName  + ' ===>' + ButtonEvent) ;
end;

procedure TMainForm.uniTriggersButtonEdit4TriggersButtonEvent(Sender: TObject;
  ButtonEvent: string);
begin
   showmessage(TBMuniTriggersButtonEdit(sender).Name  + ' ===>' + ButtonEvent) ;
end;

initialization
  RegisterAppFormClass(TMainForm);

end.
