object MainForm: TMainForm
  Left = 0
  Top = 0
  ClientHeight = 411
  ClientWidth = 752
  Caption = 'MainForm'
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  PixelsPerInch = 96
  TextHeight = 13
  object uniTriggersButtonEdit1: TBMUniTriggersButtonEdit
    Left = 40
    Top = 80
    Width = 250
    Hint = ''
    maxLength = 0
    Text = ''
    TabOrder = 1
    emptyText = 'FieldLabel  [Top]'
    ClearButton = True
    OnTriggersButtonEvent = uniTriggersButtonEdit1TriggersButtonEvent
    TriggersButtons = <
      item
        ToolTip = 'Search Records'
        ButtonName = 'Search'
        Cls = 'Search'
        ToolTipanchor = TTop
        ToolTipanchorOffeset = 0
      end>
    labelField = 'Demo Trigger [1]'
    labelAlign = LTop
    labelPad = 5
    LabelFont.Color = clRed
    LabelFont.Style = [fsBold]
    toolTip = 'Demo [1]'
    ToolTipanchor = TBottom
    ToolTipanchorOffeset = -10
    ToolTiptrackMouse = False
    minLength = 0
  end
  object uniTriggersButtonEdit2: TBMUniTriggersButtonEdit
    Left = 40
    Top = 128
    Width = 250
    Hint = ''
    maxLength = 0
    Text = ''
    TabOrder = 2
    emptyText = 'FieldLabel [Left]'
    OnTriggersButtonEvent = uniTriggersButtonEdit2TriggersButtonEvent
    TriggersButtons = <
      item
        ToolTip = 'Ricerca  records'
        ButtonName = 'Search'
        Cls = 'Search'
        ToolTipanchor = TTop
        ToolTipanchorOffeset = 0
      end
      item
        ToolTip = 'Clear  records'
        ButtonName = 'Clear'
        Cls = 'Clear'
        ToolTipanchor = TTop
        ToolTipanchorOffeset = 0
      end>
    labelField = 'Demo Trigger [2]'
    labelAlign = LLeft
    labelPad = 5
    LabelFont.Color = clLime
    LabelFont.Style = [fsBold]
    toolTip = 'Demo [2]'
    ToolTipanchor = TTop
    ToolTipanchorOffeset = 0
    ToolTiptrackMouse = False
    minLength = 0
  end
  object uniTriggersButtonEdit3: TBMUniTriggersButtonEdit
    Left = 40
    Top = 176
    Width = 250
    Hint = ''
    maxLength = 0
    Text = ''
    TabOrder = 3
    emptyText = 'FieldLabel [Right]'
    OnTriggersButtonEvent = uniTriggersButtonEdit3TriggersButtonEvent
    TriggersButtons = <
      item
        ToolTip = 'Search Demo 3'
        ButtonName = 'Search'
        Cls = 'Search'
        ToolTipanchor = TBottom
        ToolTipanchorOffeset = 4
      end>
    labelField = 'Demo Trigger [3]'
    labelAlign = LRight
    labelPad = 5
    toolTip = 'Demo 3 Left'
    ToolTipanchor = TTop
    ToolTipanchorOffeset = 0
    ToolTiptrackMouse = False
    minLength = 0
  end
  object uniTriggersButtonEdit4: TBMUniTriggersButtonEdit
    Left = 40
    Top = 224
    Width = 250
    Hint = ''
    maxLength = 0
    Text = ''
    TabOrder = 4
    emptyText = ''
    OnTriggersButtonEvent = uniTriggersButtonEdit4TriggersButtonEvent
    TriggersButtons = <
      item
        ToolTip = 'Down Button'
        ButtonName = 'Down'
        Cls = 'x-form-accept-trigger'
        ToolTipanchor = TTop
        ToolTipanchorOffeset = 0
      end>
    labelField = 'Demo Trigger [4]  '
    labelAlign = LTop
    labelPad = 5
    LabelFont.Color = clMenuHighlight
    toolTip = 'Button 4'
    ToolTipanchor = TTop
    ToolTipanchorOffeset = 0
    ToolTiptrackMouse = False
    minLength = 0
  end
  object uniTriggersButtonEdit5: TBMUniTriggersButtonEdit
    Left = 40
    Top = 272
    Width = 250
    Hint = ''
    maxLength = 0
    Text = ''
    TabOrder = 0
    emptyText = ''
    ClearButton = True
    TriggersButtons = <>
    labelField = 'Field without Trigger Button'
    labelAlign = LTop
    labelPad = 5
    toolTip = 'TriggerField without Button'
    ToolTipanchor = TTop
    ToolTipanchorOffeset = 0
    ToolTiptrackMouse = False
    minLength = 0
  end
  object UniEdit1: TUniEdit
    Left = 408
    Top = 80
    Width = 281
    Hint = ''
    Text = 'UniEdit1'
    TabOrder = 5
    ClearButton = True
    OnAjaxEvent = UniEdit1AjaxEvent
  end
  object UniEdit2: TUniEdit
    Left = 408
    Top = 128
    Width = 281
    Hint = ''
    Text = 'UniEdit2'
    TabOrder = 6
  end
  object UniEdit3: TUniEdit
    Left = 408
    Top = 176
    Width = 281
    Hint = ''
    Text = 'UniEdit3'
    TabOrder = 7
  end
  object UniEdit4: TUniEdit
    Left = 408
    Top = 224
    Width = 281
    Hint = ''
    Text = 'UniEdit4'
    TabOrder = 8
  end
  object UniEdit5: TUniEdit
    Left = 408
    Top = 272
    Width = 281
    Hint = ''
    Text = 'UniEdit5'
    TabOrder = 9
  end
  object UniLabel1: TUniLabel
    Left = 408
    Top = 24
    Width = 83
    Height = 13
    Hint = ''
    Caption = 'Standart TuniEdit'
    TabOrder = 10
  end
  object UniLabel2: TUniLabel
    Left = 40
    Top = 24
    Width = 224
    Height = 13
    Hint = ''
    Caption = 'TuniEdit with fieldLabel, allowBlank, hideLabel, '
    TabOrder = 11
  end
  object UniLabel3: TUniLabel
    Left = 40
    Top = 43
    Width = 198
    Height = 13
    Hint = ''
    Caption = 'EmptyText,ToolTip, labelAlign and trigger'
    TabOrder = 12
  end
  object uniTriggersButtonEdit6: TBMUniTriggersButtonEdit
    Left = 40
    Top = 344
    Width = 250
    Hint = ''
    maxLength = 5
    Text = ''
    TabOrder = 13
    emptyText = 'No Label- No trigger'
    ClearButton = True
    TriggersButtons = <>
    labelAlign = LTop
    labelPad = 5
    toolTip = 'No-label , No Trigger'
    ToolTipanchor = TTop
    ToolTipanchorOffeset = 0
    ToolTiptrackMouse = False
    minLength = 3
    minLengthText = 'Error : Min Length = 3'
  end
  object UniEdit6: TUniEdit
    Left = 408
    Top = 344
    Width = 121
    Hint = ''
    MaxLength = 5
    Text = 'UniEdit6'
    TabOrder = 14
  end
end
