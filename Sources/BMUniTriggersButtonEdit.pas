// ------------------------------------------------------------------------------
// BMUniTriggersButtonEdit Component
//
// Developed By: Salvatore Marullo
//
// Email: rullomare@gmail.com
//
//
// Last Update: 2015-02
// Unigui Versione 0.96.,0.97. , 098,099 Delpi XE3 ,XE4
// ------------------------------------------------------------------------------
//
// Free to use
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.



unit BMUniTriggersButtonEdit;

interface

uses
  SysUtils, Classes, Ext, Extpascal,
  uniGUIClasses, Vcl.Graphics, uniGUITypes, uniEdit, uniGUIFont, Windows,
  uniGUIClientEvents;

type
 TAnchor = (TTop, TRight, TBottom, TLeft);
 TLabelPosition = (LTop,LLeft,LRight) ;
 TTriggersButtonEvent = procedure (Sender: TObject; ButtonEvent: string) of object;

 TriggersItem = class(TCollectionItem)
  private
    FButtonName           : String ;
    FCls                  : String;
    FToolTip              : String ;
    FToolTipAnchor        : TAnchor ;
    FToolTipAnchorOffset  : integer ;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure  SetToolTipAnchor(const Value:TAnchor);
  published
    property ToolTip       : String  read FToolTip write FToolTip;
    property ButtonName    : String  read FButtonName write FButtonName;
    property Cls           : string  read FCls   write FCls;
    property ToolTipanchor : TAnchor read FToolTipAnchor  write  SetToolTipAnchor;
    property ToolTipanchorOffeset : integer read FToolTipanchorOffset  write  FToolTipAnchorOffset ;
  end;

TTriggersItems = class(TCollection)
  private
    function  GetItem(Index: Integer): TriggersItem;
    procedure SetItem(Index: Integer; const Value: TriggersItem);
  public
    function  Add: triggersItem;
    function  Insert(Index: Integer): TriggersItem;
    property  Items[Index: Integer]: TriggersItem read GetItem  write SetItem;
  end;

  TBMUniTriggersButtonEdit = class(TUniEdit)
  private
    FOnTriggersButtonEvent     :  TTriggersButtonEvent ;

    FTriggersButtons           :  TTriggersItems;
    FlabelField                :  String ;
    FlabelAlign                :  TlabelPosition ;
    FlabelPad                  :  integer ;
    FlabelSeparator            :  string ;
    FlabelFont                 :  Tunifont ;
    FemptyText                 :  String;
    FtoolTip                   :  string ;
    FToolTipanchor             :  Tanchor ;
    FToolTipanchorOffset       :  integer ;
    FToolTiptrackMouse         :  boolean ;
    FminLength                 :  integer ;
    FminLengthText             :  String ;
    FmaxLength                 :  integer ;
    FmaxLengthText             :  String ;
  protected
    procedure ConfigJSClasses(ALoading: Boolean); override;
    procedure JSEventHandler(AEventName:String;AParams:TUniStrings);override;
    procedure WebCreate; override;
    procedure LoadCompleted; override;
    procedure SetLabelFont(const Value: TuniFont);
    procedure SetLabelPosition(const Value: TLabelPosition);
    Function  GetRgbColor(color:integer) : String ;
    Function  GetLabelTextWidth(Field:String) : integer ;
    Function  GetAnchorStr( Value: Tanchor) : String ;
    Procedure SetToolTipanchor(const Value:Tanchor )  ;

    function ExtControlClass: TExtComponent;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property OnTriggersButtonEvent :  TTriggersButtonEvent    read  FOnTriggersButtonEvent write  FOnTriggersButtonEvent ;
    property TriggersButtons       : TTriggersItems           read  FTriggersButtons       write FTriggersButtons;
    property emptyText             : string                   read  FemptyText             write  FemptyText ;
    property labelField            : string                   read  FlabelField            write  FlabelField  ;
    property labelAlign            : TlabelPosition           read  FlabelAlign            write SetlabelPosition;
    property labelSeparator        : string                   read  FlabelSeparator        write FlabelSeparator;
    property labelPad              : integer                  read  FlabelPad              write FlabelPad;
    Property LabelFont             : tunifont                 read  FLabelFont             write SetLabelFont;
    Property toolTip               : string                   read  FtoolTip               write FtoolTip;
    property ToolTipanchor         : Tanchor                  read  FToolTipanchor         write  SetToolTipanchor;
    property ToolTipanchorOffeset  :  integer                 read  FToolTipanchorOffset   write  FToolTipanchorOffset ;
    property ToolTiptrackMouse     :  boolean                 read  FToolTiptrackMouse     write  FToolTiptrackMouse ;
    property minLength             : integer                  read  FminLength             write  FminLength  ;
    property minLengthText         : string                   read  FminLengthText         write  FminLengthText  ;
    property maxLength             : integer                  read  FmaxLength             write  FmaxLength  ;
    property MaxLengthText         : string                   read  FmaxLengthText         write  FmaxLengthText  ;
  end;


implementation

{ TtriggersItems }
function TTriggersItems.Add: TriggersItem;
begin
  Result := TriggersItem(inherited Add);
end;

function TTriggersItems.GetItem(Index: Integer): TriggersItem;
begin
  Result := TriggersItem(inherited Items[Index]);
end;

function TTriggersItems.Insert(Index: Integer): TriggersItem;
begin
  Result := TriggersItem(inherited Insert(Index));
end;

procedure TTriggersItems.SetItem(Index: Integer; const Value: TriggersItem);
begin
  Items[Index].Assign(Value);
end;
{ TriggersItem }

constructor TriggersItem.Create(Collection: TCollection);
begin
  inherited Create(Collection);
end;

destructor TriggersItem.Destroy;
begin
  inherited;
end;

Procedure TriggersItem.SetToolTipAnchor(const Value:TAnchor )  ;
begin
  FToolTipanchor  := value ;
end;


{ TBMUniTriggersButtonEdit }
procedure TBMUniTriggersButtonEdit.ConfigJSClasses(ALoading: Boolean);
begin
   inherited;
   JSObjects.DefaultJSClassName := 'Ext.form.field.Trigger';
end;

constructor TBMUniTriggersButtonEdit.Create(AOwner: TComponent);
begin
  inherited;
  FTriggersButtons:= TTriggersItems.Create(TriggersItem);
  FLabelField     := '';
  Flabelpad       := 5 ;
  FlabelAlign     := LTop ;
  FlabelSeparator := '' ;
  FLabelFont      := TUniFont.Create;
  Self.Width := 250;

end;
destructor TBMUniTriggersButtonEdit.Destroy;
begin
  FTriggersButtons.Destroy;
  FLabelFont.Destroy;
  inherited;
end;

Procedure TBMUniTriggersButtonEdit.SetToolTipanchor(const Value:Tanchor )  ;
begin
  FToolTipanchor  := Value ;
end;

function  TBMUniTriggersButtonEdit.ExtControlClass:TExtComponent;
begin
   inherited;
         JSObjects.DefaultJSControlName :=  'Ext.form.field.Trigger' ;
end;
procedure TBMUniTriggersButtonEdit.SetLabelPosition(const value : TLabelPosition);
begin
  FLabelAlign := Value;
end;

procedure TBMUniTriggersButtonEdit.SetLabelFont(const Value: TuniFont);
begin
  FLabelFont.Assign(Value);
end;

Function  TBMUniTriggersButtonEdit.GetAnchorStr( Value: TAnchor) : String ;
begin
  if Value = Ttop then Result := QuotedStr('top')
  else  if Value = TBottom then Result := QuotedStr('bottom')
  else  if Value = Tleft then Result := QuotedStr('left')
  else  if Value = Tright then Result := QuotedStr('right') ;
end;

Function TBMUniTriggersButtonEdit.GetLabelTextWidth(field:string) : integer ;
 var
  c: TCanvas ;
begin
    c := TCanvas.Create;
    c.Handle := GetDC(self.Handle);
    c.Font.Size    :=  flabelfont.Size ;
    c.Font.style   :=  flabelfont.Style ;
    c.Font.Name    :=  flabelfont.Name ;
    c.Font.Height  :=  flabelfont.Height ;
    result :=  c.TextWidth(field) ;
    c.Free ;
end;


Function  TBMUniTriggersButtonEdit.GetRgbColor(color:integer) : String ;
var xColor :  integer ;
   R,G,B   :  Byte ;
begin
      result := '' ;
      XColor := ColorToRGB(color);
      R := GetRValue(XColor);
      G := GetGValue(XColor);
      B := GetBValue(XColor);
      result :=  ' color : rgb(' + inttostr(r) + ' ,' + inttostr(g) + ' ,' +
                  inttostr(b) + ');' ;
end;

procedure  TBMUniTriggersButtonEdit.JSEventHandler(AEventName:String;AParams:TUniStrings);
begin
  inherited;

  if SameText (AEventName,'Triggerclick')  then begin
      if assigned(Self.OnTriggersButtonEvent ) then
       self.OnTriggersButtonEvent (self,AParams.Values['Buttons']) ;
  end ;
end;

procedure TBMUniTriggersButtonEdit.LoadCompleted;
var i               : integer ;
    StrCls          : String ;
    var Style       : string ;
    Align           : string ;
    lwidth          : integer ;
    ToolTips        : String ;

    objTrigger      : TExtFunction;
    triggers        : String ;
    TriggerToolTips : string ;
    AnchorStr       : String ;
begin

  style := '' ;
  Style := 'white-space:nowrap; font-family: ' + FlabelFont.Name +  ';';

  if    (TFontStyle.fsBold in FlabelFont.Style)   then    Style := Style +  ' font:bold ' + IntToStr(-flabelfont.Height) +'px '+ FlabelFont.Name +';'
  else                                                    Style := Style +  ' font-weight:normal;' ;
  if    (TFontStyle.fsItalic in FlabelFont.Style) then    Style := Style +  ' font-style:italic; ' ;
  Style := Style + GetRgbColor(FlabelFont.color) ;

  lwidth := GetLabelTextWidth(FlabelField) ;

  if FlabelAlign = lTop then  begin
            Align := 'top' ;
            style := Style + ' padding-bottom:' +  inttostr( labelpad) + 'px;' ;
  end
  else  if FlabelAlign = lRight then  begin
           Align        := 'right' ;
           style := Style +   'padding-right: ' + inttostr( Labelpad) + 'px;' ;
           self.Width   := self.Width + lwidth +  labelpad ;
  end
  else  if FlabelAlign = lLeft then  begin
           Align        := 'left' ;
           self.Width   := self.Width + lwidth + labelpad ;
  end;

   jsconfig('cls',['']);
   jsconfig('emptyText',[emptyText]) ;
   JSConfig('fieldLabel', [FLabelField]);
   JSConfig('labelPad', [FlabelPad]);
   JSConfig('labelAlign', [Align]);
   JSConfig('labelSeparator', [FlabelSeparator]);
   JSConfig('labelStyle', [Style]);
   JSConfig('labelWidth', [Lwidth+5]);
   jsconfig('width',[self.Width]) ;
   jsconfig('minLength',[FminLength]) ;
   if trim(self.minLengthText) <> '' then
         jsconfig('minLengthText',[self.minLengthText]) ;
   jsconfig('maxLength',[FmaxLength]) ;
   if Fmaxlength > 0  then
      JSConfig('enforceMaxLength', [True]);
   if trim(self.maxLengthText) <> '' then
     jsconfig('maxLengthText',[FmaxLengthText]) ;


   triggerTooltips := '' ;
   triggers        := '' ;
   Tooltips        := '' ;
   if FTriggersButtons.Count > 0 then begin
      for i := 0 to  FTriggersButtons.Count - 1 do begin
         if trim(FtriggersButtons.Items[i].FCls) = '' then
           StrCls := 'x-form-trigger'
         else  StrCls := 'x-form-' + FtriggersButtons.Items[i].FCls  +  '-trigger' ;

         jsconfig('trigger' + inttostr( i+1) + 'Cls',[StrCls]);
         jsconfig('itemId' +inttostr( i+1) ,  [self.FtriggersButtons.Items[i].FButtonName]);
         jsconfig('onTrigger' + inttostr( i+1) + 'Click','function() { var Button = this.itemId' +inttostr(i+1)+'; ' +
                               ' ajaxRequest(' +self.parent.name +'.'+ self.name + ',"Triggerclick",["Buttons="+Button]);}');


         triggerTooltips :=  triggerTooltips + ',{ ' +

                        '  target : ' +  uppercase( self.JSName) + '_id_trigger' +inttostr (i+1)   + ',' +
                        '  anchor: ' + getAnchorStr(  FtriggersButtons.Items[i].FToolTipanchor) + ',' +
                        '  anchorOffset : '  + inttostr( FtriggersButtons.Items[i].FtoolTipanchorOffset) + ',' +
                        '  html : ' +  quotedstr(FtriggersButtons.Items[i].FToolTip) +'}' ;
      end;

   end
   else jsconfig('hideTrigger',[true]);

   triggerTooltips :=  triggerTooltips     + '];' ;
   if  trim(FtoolTip) <> '' then

        tooltips := ' var tooltips = [ ' +
               '{ target : ' + uppercase(self.JSName) + '_id,'  +
               ' anchorToTarget : true ,' +
               ' trackMouse:  ' + lowercase(booltostr(ToolTiptrackMouse,true)) + ',' +
               ' anchor: ' + getAnchorStr(FToolTipanchor) + ',' +
               ' anchorOffset : '  + inttostr( FtoolTipanchorOffset) + ',' +
               ' html : ' + quotedstr( Ftooltip) + ' }'
   else tooltips := ' var tooltips = [ ' ;




      triggers := tooltips +  triggerTooltips +
      ' Ext.each(tooltips, function(config) { ' +
                '  Ext.create(''Ext.tip.ToolTip'', config);' +
                '  });  ' +
                ' Ext.QuickTips.init();' ;
    self.ClientEvents.ExtEvents.add('render=function render(sender, eOpts) {' + triggers + '  }');

    inherited;
end;

procedure TBMUniTriggersButtonEdit.WebCreate;
begin
  inherited;
end;

initialization

 UniAddJsLibrary('/files/trigger.js',True,[upoFolderJS,upoPlatformBoth]);



end.

